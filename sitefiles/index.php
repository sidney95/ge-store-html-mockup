<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Scripts -->
    <!-- jQuery -->
    <script   src="https://code.jquery.com/jquery-3.1.0.slim.min.js"   integrity="sha256-cRpWjoSOw5KcyIOaZNo4i6fZ9tKPhYYb6i5T9RSVJG8=" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title>GE Store</title>
</head>

<body class="index">
    <div class="page-header text-center text-uppercase">
        <h1>GE Store</h1>
        <img src="img/header.png" height="100">
    </div>
    <div class="col-xs-12 search-bar text-center">
        <form>
            <div class="search-grp">
                <input class="text-uppercase" type="text" class="form-control" placeholder="Search">
                <img src="img/search-white.png" width="15" height="15">
            </div>
        </form>
    </div>
    <div class="col-xs-12 nav-links">
        <ul>
            <a href="business-menu.html">
                <li>
                    <div>
                        Business
                        <img class="pull-right" src="img/push-arrow-black.png" width="15" height="15">                    
                    </div>
                
                </li>
            </a>
            <a href="industry-menu.html">
                <li>
                    <div>            
                        Industry
                        <img class="pull-right" src="img/push-arrow-black.png" width="15" height="15">
                    </div>
                </li>
            </a>
        </ul>
    </div>
    <div class="col-xs-12 text-center text-uppercase current-date">
        <p>Tuesday, September 6, 2016</p>
    </div>
    <div class="col-xs-12 post-feed">
        <div class="container">
            <a href="post-article-1.html">
                <div class="post-item">
                    <img class="pull-left" src="img/post1-img.png"> 
                    <div class="pull-right post-item-content">
                        <p>You'll never guess the reason robots have faces</p>
                        <span class="post-description">
                            <span class="post-type text-uppercase">topic - </span>
                            <span class="post-time text-uppercase">5 mins</span>
                        </span>
                    </div>
                </div>
            </a>
            <a href="#">
                <div class="post-item">
                    <img class="pull-left" src="img/post2-img.png">
                    <div class="pull-right post-item-content">
                        <p>3 risks poised to disrupt a fast-changing world by 2021</p>
                    </div> 
                </div>
            </a>
            <a href="#">
                <div class="post-item">
                    <img class="pull-left" src="img/post2-img.png">
                    <div class="pull-right post-item-content">
                        <p>3 risks poised to disrupt a fast-changing world by 2021</p>
                    </div> 
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 bottom-nav-menu">
        <ul>
            <li class="active" id="ge-home"><a href="index.html"><img src="img/logo.png"></a></li>
            <li><a href="business-menu.html"><img src="img/business.png"></a></li>
            <li><a href="industry-menu.html"><img src="img/industry.png"></a></li>
            <li><a href="news-feed.html"><img src="img/news.png"></a></li>
            <li><a href="info-page.html"><img src="img/info.png"></a></li>
        </ul>
    </div>
</body>

</html>